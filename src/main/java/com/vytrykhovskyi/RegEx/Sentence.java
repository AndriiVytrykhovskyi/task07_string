package com.vytrykhovskyi.RegEx;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Sentence {
    private String sentence;
    private List<Word> words;
    private List<Mark> marks;

    Sentence(String sentence) {
        this.sentence = sentence;
        this.words = new ArrayList<>();
        this.marks = new ArrayList<>();
        divByMarks();
        divByWords();
    }

    private void divByMarks() {
        Pattern pattern = Pattern.compile("[!?.,<>\":;/-]");
        Matcher matcher = pattern.matcher(sentence);
        while (matcher.find()) {
            marks.add(new Mark(sentence.charAt(matcher.start())));
        }
    }

    private void divByWords() {
        Pattern pattern = Pattern.compile("\\W+");
        Matcher matcher = pattern.matcher(sentence);
        int start = 0;
        while (matcher.find()) {
            words.add(new Word(sentence.substring(start, matcher.start())));
            start = matcher.end();
        }
    }

    List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    String getSentence() {
        return sentence;
    }

    void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public List<Mark> getMarks() {
        return marks;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }
}
