package com.vytrykhovskyi.RegEx;

class Mark {
    private char mark;

    Mark(char mark) {
        this.mark = mark;
    }

    public char getMark() {
        return mark;
    }

    public void setMark(char mark) {
        this.mark = mark;
    }
}
