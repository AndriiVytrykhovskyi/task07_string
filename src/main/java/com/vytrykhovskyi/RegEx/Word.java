package com.vytrykhovskyi.RegEx;

class Word {
    private String word;

    Word(String substring) {
        this.word = substring;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public String toString() {
        return  word + " ";
    }
}
