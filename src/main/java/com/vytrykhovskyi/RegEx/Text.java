package com.vytrykhovskyi.RegEx;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


class Text {
    private Scanner sc = new Scanner(System.in);
    private List<Sentence> sentences;

    Text() {
        sentences = new ArrayList<>();
        String text = readFile();
        getSentences(text);
    }

    private List<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(List<Sentence> sentences) {
        this.sentences = sentences;
    }

    private String readFile() {
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get("D:/ЕПАМ/Домашні/task07_String/text.txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    private void getSentences(String s) {
        String regex = "[\\.?!]";
        Pattern p1 = Pattern.compile(regex);
        Matcher m1 = p1.matcher(s);
        int start = 0;
        while (m1.find()) {
            sentences.add(new Sentence(s.substring(start, m1.start() + 1).trim()));
            start = m1.end();
        }
    }

    //1
    public void findRepeats() {
        int max = 0;
        int current = 0;
        for (int i = 0; i < sentences.size(); i++) {
            for (int j = 0; j < sentences.get(i).getWords().size(); j++) {
                for (int k = i + 1; k < sentences.size(); k++) {
                    for (int l = 0; l < sentences.get(k).getWords().size(); l++) {
                        if (sentences.get(i).getWords().get(j).getWord().equalsIgnoreCase(sentences.get(k).getWords().get(l).getWord())) {
                            current++;
                            break;
                        }
                    }
                }
                if (current > max) {
                    max = current;
                }
                current = 0;
            }
        }
        System.out.println("Count of identical word in sentence = " + max);
    }

    //2
    void sortByAmountOfWords() {
        sentences.stream()
                .sorted(Comparator.comparing(Text::getNumberOfWords))
                .forEach(s -> System.out.println(s.getSentence()));

    }

    private static int getNumberOfWords(Sentence sentence) {
        return sentence.getWords().size();
    }

    //3
    void findUniqueWordsInFirstSentence() {
        boolean isPresent;
        for (int i = 0; i < sentences.get(0).getWords().size(); i++) {
            isPresent = false;
            for (int j = 1; j < sentences.size(); j++) {
                for (int k = 0; k < sentences.get(j).getWords().size(); k++) {
                    if (sentences.get(0).getWords().get(i).getWord()
                            .equalsIgnoreCase(sentences.get(j).getWords().get(k).getWord())) {
                        isPresent = true;
                        break;
                    }
                }
            }
            if (!isPresent) {
                System.out.println(sentences.get(0).getWords().get(i).getWord());
            }
        }
    }

    //4
    public void printWordsInAskingSentences() {
        System.out.println("Set length of the words you wanna get: ");
        int length = sc.nextInt();

        Set<String> words = sentences.stream()
                .filter(sentence -> isSentenceAsking(sentence.getSentence()))
                .flatMap(sentence -> sentence.getWords().stream())
                .filter(w -> w.getWord().length() == length)
                .map(w -> w.getWord().toLowerCase())
                .collect(Collectors.toSet());

        System.out.println(words);
    }

    private boolean isSentenceAsking(String sentence) {
        Pattern p = Pattern.compile("[?$]");
        Matcher m = p.matcher(sentence);

        return m.find();
    }

    //5
    void swapWords() {
        StringBuilder string;
        for (int i = 0; i < getSentences().size(); i++) {
            Sentence s = sentences.get(i);
            string = new StringBuilder(sentences.get(i).getSentence());
            String maxLengthWord = getWordWithMaxLength(s);
            String firstVowelWord = findFirstWordStartWithVowel(s);
            if (maxLengthWord.equalsIgnoreCase(firstVowelWord) || firstVowelWord == null) {
                continue;
            }

            for (int j = 0; j < sentences.get(i).getWords().size(); j++) {
                String word = s.getWords().get(j).getWord();

                if (word.equalsIgnoreCase(maxLengthWord)) {
                    string.replace(string.lastIndexOf(word), string.lastIndexOf(word) + word.length(), firstVowelWord);

                } else {
                    if (word.equalsIgnoreCase(firstVowelWord)) {
                        string.replace(string.lastIndexOf(word), string.lastIndexOf(word) + word.length(), maxLengthWord);
                    }
                }
            }
            System.out.println(string);
        }
    }

    private boolean isStartWithVowel(String word) {
        Pattern p = Pattern.compile("[euioaEUIOA]");
        Matcher m = p.matcher(word);

        return m.find() && m.start() == 0;
    }

    private String findFirstWordStartWithVowel(Sentence sentence) {
        String word = null;
        for (int i = 0; i < sentence.getWords().size(); i++) {
            if (isStartWithVowel(sentence.getWords().get(i).getWord())) {
                word = sentence.getWords().get(i).getWord();
                break;
            }
        }
        return word;
    }

    private String getWordWithMaxLength(Sentence sentence) {
        String word = sentence.getWords().get(0).getWord();
        for (int i = 1; i < sentence.getWords().size(); i++) {
            if (sentence.getWords().get(i).getWord().length() > word.length()) {
                word = sentence.getWords().get(i).getWord();
            }
        }
        return word;
    }

    //6
    void sortWordsAlphabet() {
        List<String> allWords = new ArrayList<>();

        sentences.forEach(s -> s.getWords()
                .stream()
                .map(w -> w.getWord().toLowerCase())
                .forEach(allWords::add));

        Collections.sort(allWords);

        for (int i = 0; i < allWords.size() - 1; i++) {
            System.out.print(allWords.get(i) + " ");
            if (allWords.get(i).charAt(0) != allWords.get(i + 1).charAt(0)) {
                System.out.print("\n");
            }
        }
        System.out.print(allWords.get(allWords.size() - 1));
    }

    //7
    void sortByVowelsFrequency() {
        sentences.stream()
                .flatMap(s -> s.getWords().stream())
                .map(Word::getWord)
                .sorted(Comparator.comparing(Text::getPercentageOfVowels))
                .forEach(System.out::println);
    }

    private static double getPercentageOfVowels(String word) {
        double count = 0.0;
        Pattern p = Pattern.compile("[euioaEUIOA]");
        Matcher m = p.matcher(word);

        while (m.find()) {
            count++;
        }
        return count / word.length();
    }

    //8
    void sortWordsByFirstNonVowel() {
        sentences.stream()
                .flatMap(s -> s.getWords().stream())
                .filter(w -> isStartWithVowel(w.getWord()) && findFirstNonVowel(w.getWord()) != 0)
                .map(Word::getWord)
                .sorted(Comparator.comparing(Text::findFirstNonVowel))
                .forEach(System.out::println);
    }

    private static char findFirstNonVowel(String string) {
        Pattern p = Pattern.compile("[^euioaAEUIO]");
        Matcher m = p.matcher(string);

        while (m.find()) {
            return string.charAt(m.start());
        }
        return 0;
    }

    //9
    void sortWordsHavingAChar() {
        System.out.println("Set symbol in words to sort them: ");
        String symbol = sc.next();
        char c = symbol.charAt(0);

        sentences.stream()
                .flatMap(s -> s.getWords().stream())
                .map(w -> w.getWord().toLowerCase())
                .sorted((o1, o2) -> {
                    int result = Long.compare(amountSymbolInWord(c, o1), amountSymbolInWord(c, o2));
                    return result != 0 ? result : o1.compareTo(o2);
                })
                .forEach(System.out::println);
    }

    //10
    void frequencyOfTheUsersWord() {
        System.out.println("How many words you wanna find? ");
        int count = sc.nextInt();
        Set<String> words = new HashSet<>(count);
        System.out.println("Write them: ");
        for (int i = 0; i < count; i++) {
            words.add(sc.next().toLowerCase());
        }

        sentences.stream()
                .flatMap(s -> s.getWords().stream())
                .map(w -> w.getWord().toLowerCase())
                .filter(words::contains)
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
                .forEach(System.out::println);
    }

    //11
    void deleteWords() {
        System.out.println("Set first letter: ");
        char first = sc.next().charAt(0);

        System.out.println("Set last letter: ");
        char last = sc.next().charAt(0);


        for (int i = 0; i < getSentences().size(); i++) {
            Sentence s = sentences.get(i);
            s.setSentence(s.getSentence().replace(findWord(first, last, s), ""));
        }
        for (Sentence sentence : sentences) {
            System.out.println(sentence.getSentence().replaceAll("\\s{2,}", " ").trim());
        }
    }

    private String findWord(char start, char end, Sentence sentence) {
        List<String> words = new ArrayList<>();
        String word;
        for (int i = 0; i < sentence.getWords().size(); i++) {
            word = sentence.getWords().get(i).getWord().toLowerCase();
            if (word.charAt(0) == start && word.charAt(word.length() - 1) == end) {
                words.add(word);
            }
        }
        words.sort((o1, o2) -> o1.length() > o2.length() ? 1 : o1.compareTo(o2));
        if (words.size() == 0) {
            return "";
        } else {
            return words.get(words.size() - 1);
        }
    }

    //12
    void deleteWordsStartingNonVowel() {
        System.out.println("\nSet length of the words you want to delete: ");
        int length = sc.nextInt();

        sentences.forEach(s -> s.getWords()
                .stream()
                .filter(w -> w.getWord().length() == length && (!isStartWithVowel(w.getWord())))
                .forEach(w -> s.setSentence(s.getSentence().replace(w.getWord(), ""))));

        for (Sentence sentence : sentences) {
            System.out.println(sentence.getSentence().replaceAll("\\s{2,}", " ").trim());
        }
    }

    //13
    void sortWordsHavingAChar2() {
        System.out.println("Set symbol in words to sort them: ");
        String symbol = sc.next();
        char c = symbol.charAt(0);

        sentences.stream()
                .flatMap(s -> s.getWords().stream())
                .map(w -> w.getWord().toLowerCase())
                .sorted((o1, o2) -> {
                    int result = -Long.compare(amountSymbolInWord(c, o1), amountSymbolInWord(c, o2));
                    return result != 0 ? result : o1.compareTo(o2);
                })
                .forEach(System.out::println);
    }

    private long amountSymbolInWord(Character symbol, String word) {
        return new String(word.toCharArray()).chars()
                .mapToObj(i -> (char) i)
                .filter(character -> character == symbol)
                .count();
    }

    //14
    void findPalindromeWithMaxLength() {
        Word maxP = sentences.stream()
                .flatMap(s -> s.getWords().stream())
                .filter(word -> isPalindrome(word.getWord()))
                .max(Comparator.comparingInt(o -> o.getWord().length()))
                .get();

        System.out.println(maxP);
    }

    private boolean isPalindrome(String word) {
        StringBuilder reverseSting = new StringBuilder("");
        for (int i = word.length() - 1; i >= 0; i--) {
            reverseSting.append(word.charAt(i));
        }
        return word.equalsIgnoreCase(reverseSting.toString());
    }

    //15
    void replaceWordWithoutFirstAndLastLetter() {
        sentences.forEach(s -> s.getWords()
                .forEach(w -> s.setSentence(s.getSentence().replace(w.getWord(), deleteFirstAndLastChar(w.getWord().toLowerCase()))
                )));

        for (Sentence sentence : sentences) {
            System.out.println(sentence.getSentence().replaceAll("\\s{2,}", " ").trim());
        }

    }

    private String deleteFirstAndLastChar(String word) {
        int i = 0;
        StringBuilder newWord = new StringBuilder("");
        while (i < word.length()) {
            char first = word.charAt(0);
            char last = word.charAt(word.length() - 1);
            if (!(word.charAt(i) == first || word.charAt(i) == last)) {
                newWord.append(word.charAt(i));
            }
            i++;
        }
        return newWord.toString();
    }

    //16
    void replaceWordsByAnotherWord() {
        System.out.println("Text has " + sentences.size() + " sentences. \nSet number of string u wanna to do changes: ");
        int x;

        do {
            x = sc.nextInt();
            if (x < 1 || x > sentences.size()) {
                System.out.println("Wrong number, repeat");
            } else {
                break;
            }
        } while (true);

        --x;

        System.out.println("Set length of the word u w wanna to replace: ");
        int length = sc.nextInt();

        System.out.println("Set string to replace: ");
        String replace = sc.next();

        System.out.println("Sentence before changes:");
        System.out.println(sentences.get(x).getSentence());
        String word;
        StringBuilder newString = new StringBuilder(sentences.get(x).getSentence());

        for (int i = 0; i < sentences.get(x).getWords().size(); i++) {
            word = sentences.get(x).getWords().get(i).getWord();
            if (word.length() == length) {
                newString.replace(newString.indexOf(word), newString.indexOf(word) + length, replace);
            }
        }
        System.out.println("Sentence after changes: ");
        System.out.println(newString);
    }
}




