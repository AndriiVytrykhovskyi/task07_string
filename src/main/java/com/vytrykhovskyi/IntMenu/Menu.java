package com.vytrykhovskyi.IntMenu;

import java.util.*;

public class Menu {
    private Map<String, String> menu;
    private Map<String, Runnable> menuMethods;
    private static Scanner sc = new Scanner(System.in);
    private Locale locale;
    private ResourceBundle bundle;


    Menu() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        createMenu();

        menuMethods = new LinkedHashMap<>();

        menuMethods.put("1", this::testStringUtils);
        menuMethods.put("2", this::internationalizeMenuJapan);
        menuMethods.put("3", this::internationalizeMenuUkrainian);
        menuMethods.put("4", this::internationalizeMenuEnglish);
        menuMethods.put("5", this::internationalizeMenuFrance);
        menuMethods.put("Q", this::exit);

    }

    private void createMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("Q", bundle.getString("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.keySet()) {
            System.out.println(menu.get(str));
        }
    }

    void show() {
        String key;
        do {
            outputMenu();
            System.out.println("Enter menu point:");
            key = sc.next().toUpperCase();
            try {
                menuMethods.get(key).run();
            } catch(Exception e){
                if(!key.equals("Q")){
                    System.out.println("Wrong menu point");
                }
            }
        } while (true);
    }

    private void testStringUtils() {
        StringUtils stringUtils = new StringUtils();
        stringUtils.add("Andrii", 100, 't', 'a', 's', 'k', 42.2);
        System.out.println(stringUtils.convertToString());
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        createMenu();
        show();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        createMenu();
        show();
    }

    private void internationalizeMenuJapan() {
        locale = new Locale("ja");
        bundle = ResourceBundle.getBundle("Menu", locale);
        createMenu();
        show();
    }


    private void internationalizeMenuFrance() {
        locale = new Locale("fr");
        bundle = ResourceBundle.getBundle("Menu", locale);
        createMenu();
        show();
    }

    private void exit() {
        System.exit(0);
    }
}


