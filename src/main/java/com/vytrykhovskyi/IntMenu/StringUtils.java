package com.vytrykhovskyi.IntMenu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringUtils {
    private List<Object> objectList = new ArrayList<>();

    void add(Object... object) {
        Collections.addAll(objectList, object);
    }

    String convertToString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Object anObjectList : objectList) {
            stringBuilder.append(anObjectList).append(" ");
        }
        return stringBuilder.toString();
    }
}
